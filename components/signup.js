// components/signup.js

import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, Button, Alert, ActivityIndicator, TouchableOpacity } from 'react-native';
import firebase from '../database/firebase';


export default class Signup extends Component {

  constructor() {
    super();
    this.state = {
      displayName: '',
      email: '',
      password: '',
      isLoading: false
    }
  }

  updateInputVal = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  registerUser = () => {
    if (this.state.email === '' && this.state.password === '') {
      Alert.alert('Enter details to signup!')
    } else {
      this.setState({
        isLoading: true,
      })
      firebase
        .auth()
        .createUserWithEmailAndPassword(this.state.email, this.state.password)
        .then((res) => {
          res.user.updateProfile({
            displayName: this.state.displayName
          })
          console.log('User registered successfully!')
          this.setState({
            isLoading: false,
            displayName: '',
            email: '',
            password: ''
          })
          this.props.navigation.navigate('Login')
        })
        .catch(error => this.setState({ errorMessage: error.message }))
    }
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E" />
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <View style={{ flex: 0.75, marginTop: 30 }}>
          <Text style={{ fontSize: "36", fontWeight: "700" }}>Sign up</Text>
          <Text style={{ fontSize: '16', color: "#474A57", marginTop: 10 }}>You have to create new account if you really want.</Text>

          <View style={{ marginTop: 20 }}>

            <TextInput
              style={[styles.inputStyle, { height: 56 }]}
              placeholder="Name"
              placeholderTextColor="#18191F"
              value={this.state.displayName}
              onChangeText={(val) => this.updateInputVal(val, 'displayName')}
            />
            <TextInput
              style={[styles.inputStyle, { height: 56 }]}
              placeholder="Email"
              placeholderTextColor="#18191F"
              value={this.state.email}
              onChangeText={(val) => this.updateInputVal(val, 'email')}
            />
            <TextInput
              style={[styles.inputStyle, { height: 56 }]}
              placeholder="Password"
              placeholderTextColor="#18191F"
              value={this.state.password}
              onChangeText={(val) => this.updateInputVal(val, 'password')}
              maxLength={15}
              secureTextEntry={true}
            />
          </View>
          <View style={styles.prebutton}>
            <TouchableOpacity style={[styles.button, { height: 60 }]} onPress={() => this.registerUser()}>
              <Text style={{ color: "#000", fontSize: "21", fontWeight: "700" }}>Sign up</Text>
            </TouchableOpacity>
          </View>

          <Text
            style={styles.loginText}
            onPress={() => this.props.navigation.navigate('Login')}>
            Already have account? <Text style={{ color: "red" }}>Click here to login</Text>
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    padding: 35,
    backgroundColor: '#fff'
  },
  inputStyle: {
    width: '100%',
    height: 40,
    borderColor: "#18191F",
    borderWidth: 2,
    borderRadius: 16,
    marginBottom: 20,
    fontSize: 16,
    padding: 20,
    shadowColor: "#000000",
    elevation: 3,
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowOffset: {
      height: 0,
      width: 0
    }
  },
  loginText: {
    color: '#000',
    marginTop: 25,
    textAlign: 'center'
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  button: {
    backgroundColor: '#F59D9D',
    borderColor: "#F59D9D",
    borderRadius: 16,
    alignItems: "center",
    justifyContent: "center",
    width: "90%",
    height: 42,
    borderWidth: 1,
    shadowColor: "#000000",
    elevation: 3,
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowOffset: {
      height: 0,
      width: 0
    }
  },
  prebutton: {
    width: "110%",
    justifyContent: "center",
    alignSelf: "center",
    alignContent: "center",
    alignItems: "center",
    marginTop: 10,
  },
});