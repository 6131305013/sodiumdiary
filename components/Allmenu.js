import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, Button, Alert, ActivityIndicator, FlatList, Dimensions, KeyboardAvoidingView } from 'react-native';
import firebase from '../database/firebase';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import { LinearGradient } from 'expo-linear-gradient';
const { height, width } = Dimensions.get("window");

export default class Allmenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            items: [],
            user: null,
            displayname: '',
            CountCal: 0,
            count: 0,
            Records: '',
            test: domo,
            breakfastMeal: [],
            lunchMeal: [],
            dinnerMeal: [],
            refreshing: false
        }
    }
    pulldata = () => {
        const user = firebase.auth().currentUser
        var DisplayName = user.displayName
        var date = new Date().getDate(); //Current Date
        var month = new Date().getMonth() + 1; //Current Month
        var year = new Date().getFullYear();
        var record = date + '-' + month + '-' + year;
        let item = { breakfast: {}, dinner: {}, lunch: {} };
        let itemed = { breakfast: [], dinner: [], lunch: [] };
        firebase.database().ref('Storage' + '/' + DisplayName + '/' + record + '/').on('value', snapshot => {
            snapshot.forEach((childSnapshot) => {
                let childKey = childSnapshot.key;
                let data = childSnapshot.val();
                item[childKey] = data
            });
        })
        console.log(item)
        for (let meals in item) {
            console.log(meals)
            for (let x in item[meals]) {
                console.log(x)
                itemed[meals].push({
                    meal: item[meals][x].meal,
                    sodium: item[meals][x].sodium
                })
            }
        }
        console.log(itemed.breakfast)
        this.setState({
            items: itemed,
            refreshing: false
        })
    }
    handleRefresh = () => {
        this.setState({ refreshing: true }, () => {
            this.pulldata()
        })
    }

    componentDidMount() {
        this.pulldata()
    }
    deleteData(key) {
        var date = new Date().getDate(); //Current Date
        var month = new Date().getMonth() + 1; //Current Month
        var year = new Date().getFullYear();
        var record = date + '-' + month + '-' + year;
        const user = firebase.auth().currentUser
        var DisplayName = user.displayName
        console.warn(key)
        firebase.database().ref('Storage' + '/' + DisplayName + '/' + record + '/' + key).remove();
        this.componentDidMount();
    }

    createTwoButtonAlert = (key) =>
        Alert.alert(
            "Confirmed",
            "Are you sure?",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "OK", onPress: () => this.deleteData(key) }
            ],
            { cancelable: false }
        );

    render() {

        return (
            <KeyboardAvoidingView behavior="padding">
                <View style={styles.container}>

                    <View style={styles.title}>
                        <Text style={{ fontSize: 22, fontWeight: "700" }}>รายการอาหารวันนี้</Text>
                    </View>

                    <View style={styles.BgBottom}>
                        <View>
                            <Text style={{ fontSize: 20, fontWeight: "400", paddingHorizontal: 30, marginTop: 10 }}>รายการอาหารเช้า</Text>
                            <View style={{ padding: 12, height: 150 }}>

                                <FlatList
                                    data={this.state.items.breakfast}
                                    renderItem={({ item }) => {
                                        return (
                                            <LinearGradient
                                                colors={['#c3bef0', '#cadefc']}
                                                style={{
                                                    flex: 1,
                                                    paddingVertical: 10,
                                                    paddingHorizontal: 10,
                                                    flexDirection: 'row',
                                                    alignItems: 'center',
                                                    borderRadius: 10,
                                                    marginTop: 5,
                                                    justifyContent: "center"
                                                }}
                                                start={{ x: 0, y: 1 }} end={{ x: 1, y: 0 }}
                                            >
                                                <View style={{ height: 60, justifyContent: "center" }}>
                                                    {/* <Image source={{ uri: item.data.icon }} style={styles.Image} /> */}

                                                    <Text>เมนู : {item.meal} มีโซเดียมอยู่ {item.sodium} มิลลิกรัม</Text>

                                                    {/* <TouchableOpacity onPress={() => this.createTwoButtonAlert(item.childKey)}>
                                                        <Text style={{ color: 'white', fontSize: 20 }}>Remove</Text>
                                                    </TouchableOpacity> */}
                                                </View>
                                            </LinearGradient>
                                        )
                                    }

                                    }
                                    refreshing={this.state.refreshing}
                                    onRefresh={this.handleRefresh}
                                />

                            </View>
                        </View>
                    </View>
                    <View>
                        <Text style={{ fontSize: 20, fontWeight: "400", paddingHorizontal: 30 }}>รายการอาหารกลางวัน</Text>
                        <View style={{ padding: 12, height: 150 }}>

                            <FlatList
                                data={this.state.items.lunch}
                                renderItem={({ item }) => {
                                    return (
                                        <LinearGradient
                                            colors={['#c3bef0', '#cadefc']}
                                            style={{
                                                flex: 1,
                                                paddingVertical: 10,
                                                paddingHorizontal: 10,
                                                flexDirection: 'row',
                                                alignItems: 'center',
                                                borderRadius: 10,
                                                marginTop: 5,
                                                justifyContent: "center"
                                            }}
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 0 }}
                                        >
                                            <View style={{ height: 60, justifyContent: "center" }}>
                                                {/* <Image source={{ uri: item.data.icon }} style={styles.Image} /> */}

                                                <Text>เมนู : {item.meal} มีโซเดียมอยู่ {item.sodium} มิลลิกรัม</Text>

                                                {/* <TouchableOpacity onPress={() => this.createTwoButtonAlert(item.childKey)}>
                                                        <Text style={{ color: 'white', fontSize: 20 }}>Remove</Text>
                                                    </TouchableOpacity> */}

                                            </View>
                                        </LinearGradient>
                                    )
                                }

                                }
                                refreshing={this.state.refreshing}
                                onRefresh={this.handleRefresh}
                            />

                        </View>
                    </View>

                    <View>
                        <Text style={{ fontSize: 20, fontWeight: "400", paddingHorizontal: 30 }}>รายการอาหารเย็น</Text>
                        <View style={{ padding: 12, height: 150 }}>

                            <FlatList
                                data={this.state.items.dinner}
                                renderItem={({ item }) => {
                                    return (
                                        <LinearGradient
                                            colors={['#c3bef0', '#cadefc']}
                                            style={{
                                                flex: 1,
                                                paddingVertical: 10,
                                                paddingHorizontal: 10,
                                                flexDirection: 'row',
                                                alignItems: 'center',
                                                borderRadius: 10,
                                                marginTop: 5,
                                                justifyContent: "center"
                                            }}
                                            start={{ x: 0, y: 1 }} end={{ x: 1, y: 0 }}
                                        >
                                            <View style={{ height: 60, justifyContent: "center" }}>
                                                {/* <Image source={{ uri: item.data.icon }} style={styles.Image} /> */}

                                                <Text>เมนู : {item.meal} มีโซเดียมอยู่ {item.sodium} มิลลิกรัม</Text>
                                                {/* <TouchableOpacity onPress={() => this.createTwoButtonAlert(item.childKey)}>
                                                        <Text style={{ color: 'white', fontSize: 20 }}>Remove</Text>
                                                    </TouchableOpacity> */}
                                            </View>
                                        </LinearGradient>
                                    )
                                }

                                }
                                refreshing={this.state.refreshing}
                                onRefresh={this.handleRefresh}
                            />

                        </View>
                    </View>
                </View>
            </KeyboardAvoidingView>

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        display: "flex",
        alignItems: 'center',
        backgroundColor: '#ffb6b9'
    },
    textStyle: {
        fontSize: 15,
        marginBottom: 20
    },
    item: {
        padding: 10,
        fontSize: 12,
        fontWeight: 'bold',
        height: 35,
        color: 'white'
    },
    list: {
        flexDirection: 'row',
        padding: 20,
        alignItems: 'center',
        borderRadius: 25,
        borderBottomColor: 'grey',
        borderBottomWidth: 2,
        backgroundColor: '#5AB968',
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 5
    },
    listselect: {
        flexDirection: 'row',
        padding: 10,
        alignItems: 'center',
        borderRadius: 25,
        borderBottomColor: 'grey',
        borderBottomWidth: 2,
        backgroundColor: '#5AB968',
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 5,
        borderLeftWidth: 65,
        borderRightWidth: 65,
        borderColor: '#F4DADA',
        marginTop: 5
    },
    Image: {
        width: 80,
        height: 80,
        alignContent: 'center'
    },
    Activitylist: {
        flexDirection: 'row',
        padding: 20,
        alignItems: 'center',
        borderRadius: 25,
        borderBottomColor: 'grey',
        borderBottomWidth: 2,
        backgroundColor: '#5AB968',
    },
    Time: {
        alignItems: 'center',
        borderRadius: 15,
    },
    title: {
        alignItems: "center",
        alignSelf: "center",
        justifyContent: "center",
        marginTop: 20,
        marginBottom: 20
    },
    BgBottom: {
        backgroundColor: '#ffffff',
        flex: 1,
        width: width - 25,
        height: 15,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        elevation: 1
    }
});

const domo = [{
    'meal': 'ข้าว',
    'sodium': 123
}]