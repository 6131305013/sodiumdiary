// components/dashboard.js

import React, { Component } from 'react';
import { StyleSheet, View, Text, Button, StatusBar, Dimensions, KeyboardAvoidingView } from 'react-native';
import firebase from '../database/firebase';
import { Card, colors } from 'react-native-elements';

import { LineChart } from "react-native-chart-kit";
import { TouchableOpacity } from 'react-native-gesture-handler';

const { height, width } = Dimensions.get("window");

export default class Dashboard extends React.Component {
  constructor() {
    super();
    this.state = {
      uid: '',
      bfMeal: [],
      bfsodium: 0,
      lMeal: [],
      lsodium: 0,
      dnMeal: [],
      dnsodium: 0,
      restsodium: 0,
      totalsodium: 0,
      chartData: {},
      currentmeal: ''
    }
  }
  componentWillMount() {
    this.getChartData();
  }
  getChartData() {
    let datastore = [];
    for (let i = 0; i < 5; i++) {
      let date = new Date(Date.now() - (864e5 * i));
      let dd = date.getDate();
      let mm = date.getMonth() + 1;
      let yy = date.getFullYear();
      let stime = dd + "/" + mm + "/" + yy;
      datastore.push(stime);
    }
    // datastore.reverse();
    const user = firebase.auth().currentUser
    let DisplayName = user.displayName
    let date = new Date();
    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();
    let record = day + '-' + month + '-' + year;
    let item = [];
    // let item = [2,3,4,5,6];

    // ดึงค่ามาไว้ใช้ในการทำกราฟ --------------------------------------------------------------------------------------------------------
    firebase.database().ref('History/' + DisplayName + '/').on('value', snapshot => {
      snapshot.forEach((child) => {
        let object = child.val().sodium;
        for (let i = 0; i < 5; i++) {
          let date = new Date(Date.now() - (864e5 * i));
          let dd = date.getDate();
          let mm = date.getMonth() + 1;
          let yy = date.getFullYear();
          let stime = dd + "-" + mm + "-" + yy;
          if (child.key === stime) {
            item[i] = object;
          }
          else if (item[i] === undefined) {
            item[i] = 0
          }
        }
      })
    })
    // var resetArray = (arr) => {
    //       for (var i = 0; i < arr.length; i++) {
    //         if (typeof (arr[i]) === "undefined") {
    //           arr[i] = 0
    //         }
    //       }
    //     }
    //     resetArray(item);
    item.reverse()
    console.log(item)
    // Ajax calls here
    this.setState({
      chartData: {
        labels: datastore,
        datasets: [
          {
            data: item
          }
        ]
      }
    });
  }


  componentDidMount() {
    const user = firebase.auth().currentUser
    var DisplayName = user.displayName
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear();
    var record = date + '-' + month + '-' + year;
    let currentMeal = '';
    let Fbsodium = 0;
    let Lsodium = 0;
    let Dnsodium = 0;
    let Totalsodium = 0;
    let Restsodium = 0;
    let todayMeal = [];
    firebase.database().ref('Storage' + '/' + DisplayName + '/' + record + '/').on('value', snapshot => {
      snapshot.forEach((childSnapshot) => {
        currentMeal = childSnapshot.key;
        console.warn('data is ' + currentMeal);
        firebase.database().ref('Storage' + '/' + DisplayName + '/' + record + '/' + currentMeal).on('value', datashot => {
          datashot.forEach((child) => {
            // console.log(child.val())
            if (child.val().currentmeal == 'breakfast') {
              console.log('breakfast is '
                + child.val())
              this.setState({ bfMeal: child.val() })
              Fbsodium += child.val().sodium;
              // console.log(Fbsodium)
            } else if (child.val().currentmeal == 'lunch') {
              console.log('lunch is ' + child.val())
              this.setState({ lMeal: child.val() })
              Lsodium += child.val().sodium;
              // console.log(Lsodium)
            } else if (child.val().currentmeal == 'dinner') {
              this.setState({ dnMeal: child.val() })
              Dnsodium += child.val().sodium;
              // console.log(Dnsodium)
            }
            todayMeal.push(child.val())
            Totalsodium = Fbsodium + Lsodium + Dnsodium
            Restsodium = 2000 - Totalsodium
            this.setState({
              bfsodium: Fbsodium,
              lsodium: Lsodium,
              dnsodium: Dnsodium,
              totalsodium: Totalsodium,
              restsodium: Restsodium
            })
            firebase.database().ref('History/' + DisplayName + '/' + record).set({
              Date: record,
              sodium: Totalsodium
            })
          })
        })
      });
    });
  }


  // signOut = () => {
  //   firebase.auth().signOut().then(() => {
  //     this.props.navigation.navigate('Login')
  //   })
  //   .catch(error => this.setState({ errorMessage: error.message }))
  // }  

  render() {
    return (
      <KeyboardAvoidingView behavior="padding">
        <View style={styles.container}>
          <StatusBar barStyle="light-content" />
          <View style={styles.bg}>
            <View style={{ flexDirection: 'row', marginTop: 15, alignSelf: "center" }}>

              <TouchableOpacity onPress={() => this.props.navigation.navigate('รายการอาหารวันนี้')}>
                <Card title={'อาหารเช้า'}
                  titleStyle={{ textAlign: 'center' }}
                  containerStyle={[
                    styles.cardMeal,
                    { backgroundColor: '#D1EAA3' }
                  ]}
                >
                  <Text style={{ textAlign: 'center' }}>{this.state.bfsodium} มล.</Text>
                </Card>
              </TouchableOpacity>


              <TouchableOpacity onPress={() => this.props.navigation.navigate('รายการอาหารวันนี้')}>
                <Card title={' อาหารกลางวัน '}
                  titleStyle={{ textAlign: 'center' }}
                  containerStyle={[
                    styles.cardMeal,
                    { backgroundColor: '#F6FBB4' }
                  ]}
                >
                  <Text style={{ textAlign: 'center' }}>{this.state.lsodium} มล.</Text>
                </Card>
              </TouchableOpacity>


              <TouchableOpacity onPress={() => this.props.navigation.navigate('รายการอาหารวันนี้')}>
                <Card title={'   อาหารเย็น   '}
                  titleStyle={{ textAlign: 'center', color: "white" }}
                  containerStyle={[
                    styles.cardMeal,
                    { backgroundColor: '#00005C' }
                  ]}
                >
                  <Text style={{ textAlign: 'center', color: 'white' }}>{this.state.dnsodium} มล.</Text>
                </Card>
              </TouchableOpacity>
            </View>

            <Text style={styles.subdetail}>**สามารถเพิ่มอาหารได้โดยการกดมื้ออาหาร</Text>


            <Card
              containerStyle={[
                styles.cardDetail,
                { backgroundColor: '#F4DADA' }
              ]}
            >
              <Text style={{ flexDirection: 'row', alignSelf: "center" }}>
                <Text style={{ fontWeight: 'bold' }}>วันนี้สามารถรับโซเดียมได้อีก</Text>
                <Text>          {this.state.restsodium} มิลลิกรัม</Text>
              </Text>
            </Card>


            <Text style={styles.subdetail}>ไม่ควรทานโซเดียมเกิน 2000 มิลลิกรัมต่อวัน</Text>


            <Card
              containerStyle={[
                styles.cardDetail,
                { backgroundColor: '#BEEBE9' }
              ]}
            >
              <Text style={{ flexDirection: 'row', alignSelf: "center" }}>
                <Text style={{ fontWeight: 'bold' }}>โซเดียมที่ได้รับในวันนี้</Text>
                <Text>               {this.state.totalsodium} มิลลิกรัม</Text>
              </Text>
            </Card>
          </View>

          <View style={styles.chart}>
            <LineChart
              style={{
                marginVertical: 8,
                borderRadius: 10,
                height: 25
              }}
              data={this.state.chartData}
              width={Dimensions.get("window").width}
              height={300}
              chartConfig={{
                backgroundColor: "#e26a00",
                backgroundGradientFrom: "#fb8c00",
                backgroundGradientTo: "#ffa726",
                decimalPlaces: 2, // optional, defaults to 2dp
                strokeWidth: 2,
                color: (opacity = 1) => `rgba(51, 153, 255, ${opacity})`,
                labelColor: (opacity = 1) => `rgba(0,0,0, ${opacity})`,
                style: {
                  borderRadius: 10
                }
              }}

            />
          </View>

        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  textStyle: {
    fontSize: 15,
    marginBottom: 20
  },
  bg: {
    backgroundColor: "#F59D9D",
    flex: 1,
    borderBottomLeftRadius: 50,
    borderBottomRightRadius: 50,
    shadowColor: "#000000",
    elevation: 3,
    shadowOpacity: 0.5,
    shadowRadius: 3,
    shadowOffset: {
      height: 0,
      width: 0
    },
    alignSelf: "stretch",

  },
  cardDetail: {
    height: 60,
    width: 350,
    padding: 20,
    borderRadius: 12,
    shadowColor: "#000000",
    elevation: 3,
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowOffset: {
      height: 0,
      width: 0
    },
    alignSelf: "center"
  },
  chart: {
    marginTop: 5,
    flex: 1
  },
  cardMeal: {
    margin: 5,
    borderRadius: 15,
    shadowColor: "#000000",
    elevation: 3,
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowOffset: {
      height: 0,
      width: 0
    }
  },
  subdetail: {
    alignSelf: "center",
    marginTop: 2,
    color: '#585353'
  }
});

