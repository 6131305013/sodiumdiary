// components/login.js

import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, Button, Alert, ActivityIndicator, Dimensions, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import firebase from '../database/firebase';

const { height, width } = Dimensions.get("window");

export default class Login extends Component {

  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      isLoading: false
    }
  }

  updateInputVal = (val, prop) => {
    const state = this.state;
    state[prop] = val;
    this.setState(state);
  }

  userLogin = () => {
    if (this.state.email === '' && this.state.password === '') {
      Alert.alert('Enter details to signin!')
    } else {
      this.setState({
        isLoading: true,
      })
      firebase
        .auth()
        .signInWithEmailAndPassword(this.state.email, this.state.password)
        .then((res) => {
          console.log(res)
          console.log('User logged-in successfully!')
          this.setState({
            isLoading: false,
            email: '',
            password: ''
          })
          this.props.navigation.navigate('Homepage')
        })
        .catch(error => this.setState({ errorMessage: error.message }))
    }
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={styles.preloader}>
          <ActivityIndicator size="large" color="#9E9E9E" />
        </View>
      )
    }
    return (
      <KeyboardAvoidingView behavior="padding">
        <View style={styles.container}>
          <View style={styles.card}>
            <Text style={styles.text}> Login </Text>
            <View style={{ marginTop: 30 }}>
              <TextInput
                style={[styles.inputStyle, { height: 56 }]}
                placeholder="Email address"
                placeholderTextColor="#18191F"
                value={this.state.email}
                onChangeText={(val) => this.updateInputVal(val, 'email')}
              />

              <TextInput
                style={[styles.inputStyle, { height: 56 }]}
                placeholder="Password"
                placeholderTextColor="#18191F"
                value={this.state.password}
                onChangeText={(val) => this.updateInputVal(val, 'password')}
                maxLength={15}
                secureTextEntry={true}
              />
            </View>


            <View style={styles.prebutton}>
              <TouchableOpacity style={[styles.button, { height: 60 }]} onPress={() => this.userLogin()}>
                <Text style={{ color: "#fff", fontSize: "21", fontWeight: "700" }}>Sign in</Text>
              </TouchableOpacity>
            </View>

            <Text
              style={styles.loginText}
              onPress={() => this.props.navigation.navigate('Signup')}>
              You are new?<Text style={{ color: 'red' }}>Create new</Text>
            </Text>
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    padding: 35,
    backgroundColor: '#fff'
  },
  inputStyle: {
    width: '75%',
    height: 40,
    borderColor: "#18191F",
    borderWidth: 2,
    borderRadius: 16,
    marginBottom: 20,
    fontSize: 16,
    padding: 20,
    alignSelf: "center",
    shadowColor: "#000000",
    elevation: 3,
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowOffset: {
      height: 0,
      width: 0
    }
  },
  loginText: {
    color: '#000',
    marginTop: 70,
    textAlign: 'center',
    fontSize: 18
  },
  preloader: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  card: {
    backgroundColor: '#F59D9D',
    flex: 0.75,
    borderTopLeftRadius: 41,
    borderTopRightRadius: 41,
    borderBottomLeftRadius: 41,
    borderBottomRightRadius: 41,
    marginBottom: 20,
    marginTop: 20,
    width: width - 25,
    alignSelf: 'center',
    shadowColor: "#000000",
    elevation: 3,
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowOffset: {
      height: 0,
      width: 0
    }
  },
  text: {
    fontWeight: '700',
    fontSize: 36,
    alignSelf: 'center',
    marginTop: 25
  },
  button: {
    backgroundColor: '#000',
    borderRadius: 16,
    alignItems: "center",
    justifyContent: "center",
    width: "70%",
    height: 42,
    borderWidth: 1,
    shadowColor: "#000000",
    elevation: 3,
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowOffset: {
      height: 0,
      width: 0
    }
  },
  prebutton: {
    width: "110%",
    justifyContent: "center",
    alignSelf: "center",
    alignContent: "center",
    alignItems: "center",
    marginTop: 30,
  }
});