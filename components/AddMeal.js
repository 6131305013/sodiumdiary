import React, { Component } from 'react';
import { StyleSheet, View, Text, Button, FlatList, TouchableOpacity, ScrollView, SafeAreaView, TextInput, Dimensions, Image, Alert, KeyboardAvoidingView } from 'react-native';
import firebase from '../database/firebase';
import { Card } from 'react-native-elements';
import _ from 'lodash'

const { height, width } = Dimensions.get("window");

export default class AddMeal extends Component {
  constructor() {
    super();
    this.state = {
      items: [],
      fullitems: [],
      SelectedMeal: 'ไม่ระบุ',
      SelectedSodium: 'ไม่ระบุ',
      SelectedMealPhoto: '',
      currentMeal: '',
      breakfastpress: true,
      lunchpress: true,
      dinnerpress: true,
      query: ''
    }
  }

  async AddMeal() {
    if (this.state.currentMeal == '') {
      Alert.alert("โปรดเลือกมื้ออาหาร")
    }
    if (this.state.SelectedSodium == 'ไม่ระบุ' && this.state.SelectedMeal == 'ไม่ระบุ') {
      Alert.alert("โปรดเลือกเมนูอาหาร")
    }
    if (this.state.currentMeal != '' && this.state.SelectedSodium != 'ไม่ระบุ' && this.state.SelectedMeal != 'ไม่ระบุ') {
      let meal = this.state.SelectedMeal;
      let currentmeal = this.state.currentMeal;
      let sodium = this.state.SelectedSodium;
      const user = firebase.auth().currentUser
      var DisplayName = user.displayName
      var hours = new Date().getHours();
      var hours = new Date().getHours();
      var minutes = new Date().getMinutes();
      var time = hours + ':' + minutes;
      var date = new Date().getDate(); //Current Date
      var month = new Date().getMonth() + 1; //Current Month
      var year = new Date().getFullYear();
      var record = date + '-' + month + '-' + year;
      firebase.database().ref('Storage' + '/' + DisplayName + '/' + record + '/' + currentmeal).push({
        meal,
        sodium,
        record,
        currentmeal,
        time,
      })
      this.props.navigation.navigate('เมนูอาหาร')
    }
  }

  SelectedMeal(item) {
    this.setState({
      SelectedMeal: item.meal,
      SelectedSodium: item.sodium,
      SelectedMealPhoto: item.photo
    })
  }
  setCurrentMeal(value) {
    this.setState({
      currentMeal: value
    })
    if (value == "breakfast") {
      this.state.breakfastpress = !this.state.breakfastpress
      this.state.lunchpress = true
      this.state.dinnerpress = true
    } else if (value == "lunch") {
      this.state.lunchpress = !this.state.lunchpress
      this.state.breakfastpress = true
      this.state.dinnerpress = true
    } else if (value == "dinner") {
      this.state.dinnerpress = !this.state.dinnerpress
      this.state.lunchpress = true
      this.state.breakfastpress = true
    }
  }

  async componentWillMount() {
    let item = [];
    await firebase.database().ref('menu/').on('value', snapshot => {
      snapshot.forEach((childSnapshot) => {
        let object = childSnapshot.val();
        item.push(object)
      })
    })
    console.log(item)
    this.setState({ items: item, fullitems: item })
  }
  createTwoButtonAlert = () =>
    Alert.alert(
      "Confirmed",
      "Are you sure?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => this.AddMeal() }
      ],
      { cancelable: false }
    );
  handleSearch = text => {
    console.log(text)
    const formatQuery = text.toLowerCase();
    const items = _.filter(this.state.fullitems, item => {
      if (item.meal.includes(formatQuery)) {
        return true
      }
      return false
    })
    this.setState({ query: formatQuery, items })
  }
  render() {

    return (
      <KeyboardAvoidingView behavior="padding">
        <View style={styles.container}>

          <SafeAreaView style={{ flex: 1 }}>
            <View style={[styles.section, styles.footer]}>
              <View style={{ marginBottom: 10 }}>
                <Text style={{
                  fontSize: 20,
                  fontWeight: "700",
                }}>เมูนูที่เลือก</Text>
              </View>
              <View style={{ flexDirection: "row", marginBottom: 5 }}>
                <TouchableOpacity onPress={() => this.setCurrentMeal("breakfast")}>
                  <View style={this.state.breakfastpress ? styles.breakfasebutton : styles.Selectbutton}>
                    <Text>มื้อเช้า</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.setCurrentMeal("lunch")}>
                  <View style={this.state.lunchpress ? styles.lunchbuttom : styles.Selectbutton}>
                    <Text>มื้อกลางวัน</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.setCurrentMeal("dinner")}>
                  <View style={this.state.dinnerpress ? styles.dinnerbutton : styles.Selectbutton}>
                    <Text>มื้อเย็น</Text>
                  </View>
                </TouchableOpacity>
              </View>
              <Image source={{ uri: this.state.SelectedMealPhoto }} style={styles.Image} />
              <View style={{ marginTop: 5 }}>
              </View>
              <View style={{ padding: 10, alignItems: 'center' }}>
                <Text style={{ fontSize: "16", fontWeight: "700" }}>เมนู : {this.state.SelectedMeal}</Text>
                <Text style={{ fontSize: "16", fontWeight: "700" }}>ค่าโซเดียม : {this.state.SelectedSodium} มิลลิกรัม</Text>
              </View>
              <TouchableOpacity
                style={[styles.button]}
                onPress={() => this.createTwoButtonAlert()}>
                <Text style={{ color: "#000000", fontSize: 21, }}>เพิ่ม</Text>
              </TouchableOpacity>
            </View>
            <Image source={{ uri: this.state.SelectedMealPhoto }} style={styles.Image} />
            <View style={{ marginTop: 5 }}>
            </View>
            <View style={{ padding: 10, alignItems: 'center' }}>
              <Text style={{ fontSize: "16", fontWeight: "700" }}>เมนู : {this.state.SelectedMeal}</Text>
              <Text style={{ fontSize: "16", fontWeight: "700" }}>ค่าโซเดียม : {this.state.SelectedSodium} มิลลิกรัม</Text>
            </View>
            <TouchableOpacity
              style={[styles.button]}
              onPress={() => this.createTwoButtonAlert()}>
              <Text style={{ color: "#000000", fontSize: 21, }}>เพิ่ม</Text>
            </TouchableOpacity>
            <TextInput style={styles.textInputStyle} placeholder="Search here..." onChangeText={this.handleSearch} />
            <View style={styles.bg}>

              <FlatList
                data={this.state.items}
                renderItem={({ item }) =>

                  <TouchableOpacity onPress={() => this.SelectedMeal(item)}>

                    <View style={styles.list}>
                      {/* <Image source={{ uri: item.Sport_Icon }} style={styles.Image} /> */}
                      <View style={{ flexDirection: 'row' }}>
                        <View>

                          <Text style={[styles.item, { color: "#000" }]}>{item.id}.{item.meal}</Text>
                          <Text style={[styles.item, { color: "#000" }]}>sodium is {item.sodium}</Text>

                        </View>

                      </View>

                    </View>
                  </TouchableOpacity>
                }
              />
            </View>

          </SafeAreaView>

        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    alignItems: 'center',
    backgroundColor: '#ffb6b9'
  },
  textStyle: {
    fontSize: 15,
    marginBottom: 20
  },
  item: {
    padding: 5,
    fontSize: 16,
    fontWeight: 'bold',
    height: 32
  },
  list: {
    flexDirection: 'row',
    padding: 5,
    borderBottomWidth: 1,
    marginBottom: 5,
    borderColor: "#fff",
    backgroundColor: "#fff",
    borderRadius: 15
  },
  listselect: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center',
    borderRadius: 25,
    borderBottomColor: 'grey',
    borderBottomWidth: 2,
    backgroundColor: '#5AB968',
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 5,
    borderLeftWidth: 65,
    borderRightWidth: 65,
    borderColor: '#5AB968',
    marginTop: 5
  },
  Image: {
    width: 250,
    height: 250,
    alignContent: 'center'
  },
  Activitylist: {
    flexDirection: 'row',
    padding: 20,
    alignItems: 'center',
    borderRadius: 25,
    borderBottomColor: 'grey',
    borderBottomWidth: 2,
    backgroundColor: '#5AB968',
  },
  Time: {
    alignItems: 'center',
    borderRadius: 25,
  },
  button: {
    backgroundColor: '#cff6cf',
    borderColor: "#cff6cf",
    borderRadius: 16,
    borderLeftWidth: 120,
    borderRightWidth: 120,
    alignItems: "center",
    justifyContent: "center",
    // width: 100,
    // height: 42,
    borderWidth: 10,
    shadowColor: "#000000",
    shadowOpacity: 0.3,
    shadowRadius: 3,
    shadowOffset: {
      height: 0,
      width: 0
    }
  },
  prebutton: {
    width: 100,
    justifyContent: "center",
    alignSelf: "center",
    alignContent: "center",
    alignItems: "center",
    marginTop: 10,
  },
  section: {
    alignSelf: "stretch"
  },
  footer: {
    flexDirection: "column",
    alignItems: "center",
    marginTop: 20,
  },
  input: {
    flex: 1,
    height: 48,
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: 6,
    marginRight: 8,
    paddingHorizontal: 8
  },
  bg: {
    flex: 1,
    width: width - 0,
    elevation: 3,
    marginTop: 10
  },
  breakfasebutton: {
    borderColor: "#cff6cf",
    backgroundColor: "#cff6cf",
    borderWidth: 15,
    marginRight: 10,
    borderRadius: 15
  },
  lunchbuttom: {
    borderColor: "#fefdca",
    backgroundColor: "#fefdca",
    borderWidth: 15,
    marginRight: 10,
    borderRadius: 15
  },
  dinnerbutton: {
    borderColor: "#6eb6ff",
    backgroundColor: "#6eb6ff",
    borderWidth: 15,
    marginRight: 10,
    borderRadius: 15
  },
  Selectbutton: {
    borderColor: "#cc3300",
    backgroundColor: "#cc3300",
    borderWidth: 15,
    marginRight: 10,
    borderRadius: 15
  },
  textInputStyle: {
    height: 40,
    borderWidth: 1,
    paddingLeft: 10,
    borderColor: '#009688',
    backgroundColor: '#FFFFFF',
  }
});