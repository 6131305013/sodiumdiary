import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, Button, Alert, ActivityIndicator, FlatList, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import firebase from '../database/firebase';

export default class History extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      items: [],
      user: null,
      displayname: '',
      CountCal: 0,
      count: 0,
      Records: '',
      show: true,
      dayList: [],
      test: domo
    }
  }
  signOut = () => {
    firebase.auth().signOut().then(() => {
      this.props.navigation.navigate('Login')
    })
      .catch(error => this.setState({ errorMessage: error.message }))
  }

  ShowHideComponent = () => {
    if (this.state.show == true) {
      this.setState({ show: false });
    } else {
      this.setState({ show: true });
    }
  };

  componentDidMount() {

    let item = [];
    const user = firebase.auth().currentUser
    var DisplayName = user.displayName
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear();
    var record = year + '-' + month + '-' + date;
    let itemed = { object: [] };
    firebase.database().ref('History' + '/' + DisplayName + '/').on('value', snapshot => {
      snapshot.forEach((childSnapshot) => {
        let key = childSnapshot.key;
        let data = childSnapshot.val();
        item.push(data);
      })
    })
    console.log(item)
    console.log(item[0])

    for (let day in item) {
      console.log(item[day])

    }
    console.log(item)

    this.setState({
      dayList: item
    })
  }


  render() {
    console.log(this.state.dayList)
    return (
      <KeyboardAvoidingView behavior="padding">
        <View style={styles.container}>
          <View style={styles.title}>
            <Text style={{ fontSize: 22, fontWeight: "700" }}>History</Text>
          </View>
          {/* รายการอาหาร */}
          <View style={{ flex: 1, height: 250 }}>
            <FlatList
              data={this.state.dayList}
              renderItem={({ item }) =>
                // <TouchableOpacity onPress={() => this.ShowHideComponent()}>
                <View style={styles.list}>
                  {/* <Image source={{ uri: item.Sport_Icon }} style={styles.Image} /> */}
                  <View style={[styles.item, { flexDirection: 'row' }]}>
                    <Text>ค่าโซเดียมของวันที่ {item.Date} คือ {item.sodium} มิลลิกรัม</Text>
                    {/* <Text style={styles.item}>{item.sodium}</Text> */}
                  </View>
                </View>
                // </TouchableOpacity>
              }
            />

            <TouchableOpacity onPress={() => this.signOut()}>
              <View style={{ padding: 10, borderRadius: 25, alignItems: 'center', borderColor: '#EFEB7A', backgroundColor: '#EFEB7A', marginLeft: 30, marginRight: 30, marginBottom: 20, marginTop: 20 }}>
                <Text style={{ fontSize: 20 }}>Signout</Text>
              </View>
            </TouchableOpacity>
          </View>
          {/* ------------------- */}

        </View>
      </KeyboardAvoidingView>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: "flex",
    alignItems: 'center',
    backgroundColor: '#ffffff'
  },
  textStyle: {
    fontSize: 15,
    marginBottom: 20
  },
  item: {
    padding: 10,
    fontSize: 14,
    fontWeight: 'bold',
    color: 'black',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center'
  },
  list: {
    flexDirection: 'row',
    padding: 20,
    alignItems: 'center',
    borderRadius: 15,
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    backgroundColor: '#769fcd',
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 5
  },
  listselect: {
    flexDirection: 'row',
    padding: 10,
    alignItems: 'center',
    borderRadius: 25,
    borderBottomColor: 'grey',
    borderBottomWidth: 2,
    backgroundColor: '#5AB968',
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 5,
    borderLeftWidth: 65,
    borderRightWidth: 65,
    borderColor: '#5AB968',
    marginTop: 5
  },
  Image: {
    width: 80,
    height: 80,
    alignContent: 'center'
  },
  Activitylist: {
    flexDirection: 'row',
    padding: 20,
    alignItems: 'center',
    borderRadius: 25,
    borderBottomColor: 'grey',
    borderBottomWidth: 2,
    backgroundColor: '#5AB968',
  },
  Time: {
    alignItems: 'center',
    borderRadius: 25,
  },
  title: {
    alignItems: "center",
    alignSelf: "center",
    justifyContent: "center",
    marginTop: 20,
    marginBottom: 20
  }
});
const domo = [{
  'meal': '11-05-20',
  'sodioum': '2000'
}]