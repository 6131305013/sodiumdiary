import * as firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyAx0dvAleSHmJQqQcYRZUTabmEiiUGDvvo",
  authDomain: "sodiumdairy.firebaseapp.com",
  databaseURL: "https://sodiumdairy.firebaseio.com",
  projectId: "sodiumdairy",
  storageBucket: "sodiumdairy.appspot.com",
  messagingSenderId: "844166408694",
  appId: "1:844166408694:web:00f9c31b2a564376de3fd7",
  measurementId: "G-QCZQLD6112"
};

firebase.initializeApp(firebaseConfig);

export default firebase;