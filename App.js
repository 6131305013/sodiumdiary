import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { AntDesign } from '@expo/vector-icons'
import { Ionicons } from '@expo/vector-icons'
import { FontAwesome5 } from '@expo/vector-icons'

import Login from './components/login';
import Signup from './components/signup';
import Dashboard from './components/dashboard';
import Allmenu from './components/Allmenu';
import History from './components/History';
import AddMeal from './components/AddMeal'

const Stack = createStackNavigator();
const BottomTab = createBottomTabNavigator();

function Allmenus() {
  return (
    <Stack.Navigator screenOptions={{
    }}>
      <Stack.Screen
        name="รายการอาหารวันนี้"
        component={Allmenu}
        options={{
          headerStyle: {
            backgroundColor: '#beebe9',
          },
          headerTitleAlign: 'center'
        },
          { headerShown: false }}
      />
    </Stack.Navigator>
  )
}

function AddMealStack() {
  return(
    <Stack.Navigator
    initialRouteName="เพิ่ม"
    screenOptions={{headerShown:false}}
    >
      <Stack.Screen
      name="เมนูอาหารทั้งหมด"
      component={Allmenus}
      options={{
        tabBarIcon: ({ tintColor }) => (
          <Ionicons
            name="md-menu"
            size={30}
            color="#tintColor"
            style={{
              marginTop: 4,
              position:"absolute"
            }}
          />
        )
      }}
      />
      <Stack.Screen
        name="เพิ่ม"
        component={AddMeal}
        options={{
          tabBarIcon: ({ tintColor }) => (
            <AntDesign
              name="pluscircle"
              size={60}
              color="#E9446A"
              style={{
                // shadowColor: "#E9446A",
                // shadowRadius: 3,
                // shadowOffset: { width: 0, height: 0 },
                // shadowOpacity: 0.5,
                position:"absolute",
                top:-30
              }}
            />
          )
        }}
      />
    </Stack.Navigator>
  )
}

function programs() {
  return (
    <BottomTab.Navigator>
      <BottomTab.Screen
        name="เมนูอาหาร"
        component={Allmenus}
        options={{
          tabBarIcon: ({ tintColor }) => (
            <Ionicons
              name="md-menu"
              size={30}
              color="#tintColor"
              style={{
                marginTop: 4,
                position:"absolute"
              }}
            />
          )
        }}
      />
      <BottomTab.Screen
        name="เพิ่ม"
        component={AddMealStack}
        options={{
          tabBarIcon: ({ tintColor }) => (
            <AntDesign
              name="pluscircle"
              size={60}
              color="#E9446A"
              style={{
                shadowColor: "#E9446A",
                shadowRadius: 3,
                shadowOffset: { width: 0, height: 0 },
                shadowOpacity: 0.5,
                position:"absolute",
                top:-30
              }}
            />
          )
        }}
      />
      <BottomTab.Screen
        name="ประวัติ"
        component={History}
        options={{
          tabBarIcon: ({ tintColor }) => (
            <FontAwesome5
              name="history"
              size={28}
              color="#tintColor"
              style={{
                marginTop: 4,
                position:"absolute"
              }}
            />
          )
        }}
      />
    </BottomTab.Navigator>
  )
}

function Homepage() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Sodium Diary"
        component={Dashboard}
        options={{
          headerStyle: {
            backgroundColor: '#F59D9D',
          },
          headerTitleAlign: 'center'
        }}
      />
      <Stack.Screen name="รายการอาหารวันนี้" component={programs} options={{
        headerStyle: {
          backgroundColor: '#F59D9D',
        },
        headerTitleAlign: 'center',
        headerTitle:"Sodium Diary"
      }} />
    </Stack.Navigator>
  )
}


function MyStack() {
  return (
    <Stack.Navigator
      initialRouteName="Login"
      screenOptions={{
        headerTitleAlign: 'center',
        headerStyle: {
          backgroundColor: '#3740FE',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
        headerShown: false
      }}>
      <Stack.Screen
        name="Signup"
        component={Signup}
        options={{ title: 'Signup', }}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={
          { title: 'Login' },
          { headerLeft: null }
        }
      />
      <Stack.Screen
        name="Homepage"
        component={Homepage}
        options={
          { title: 'Homepage' },
          { headerLeft: null }
        }
      />
    </Stack.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <MyStack />
    </NavigationContainer>
  );
}